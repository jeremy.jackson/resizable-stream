/* eslint import/no-dynamic-require: "off" */
const expect = require('expect.js');

const ResizableStream = require(process.env.PWD);

describe('ResizableStream', () => {
  it('should default to outputting chunks the same size as the input', (done) => {
    const rs = new ResizableStream();

    const length = Math.trunc(Math.random() * 10);

    rs.on('readable', () => {
      const buf = rs.read();
      expect(buf.length).to.eql(length);
      done()
    });

    rs.write(Buffer.alloc(length));
  });

  it('should go from small chunks to one big chunk', (done) => {
    const length = 6;
    const rs = new ResizableStream(length);

    rs.on('readable', () => {
      const buf = rs.read();
      expect(buf.length).to.eql(length);
      done()
    });

    rs.write(Buffer.alloc(length / 2));
    rs.write(Buffer.alloc(length / 2));
  });

  it('should go from one big chunk to some small chunks', (done) => {
    const length = 3;
    const rs = new ResizableStream(length);

    rs.on('readable', () => {
      const buf = rs.read();
      expect(buf.length).to.eql(length);
      done()
    });

    rs.write(Buffer.alloc(length * 2));
  });
});
