const { Transform } = require('stream');

module.exports = class extends Transform {
  constructor(size, options) {
    super(options);

    this.size = size || 0;
    this._bufferArray = [];
  }

  _transform (chunk, encoding, callback) {
    let errPush = null;
    let bufPush = null;

    if (this.size === 0) {
      bufPush = chunk;
    } else {
      try {
        this._bufferArray.push(chunk);
        const buf = Buffer.concat(this._bufferArray);
        
        if (this.size <= buf.length) {
          bufPush = buf.slice(0, this.size);
          this._bufferArray = [buf.slice(this.size)];
        } else {
          this._bufferArray = [buf];
        }
      } catch (err) {
        errPush = err;
      }
    }

    return callback(errPush, bufPush);
  }
};
